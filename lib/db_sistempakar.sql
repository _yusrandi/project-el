-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 27, 2017 at 05:46 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sistempakar`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(10) NOT NULL,
  `Nama_admin` varchar(25) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `email` varchar(35) NOT NULL,
  `Username` varchar(25) NOT NULL,
  `Password` varchar(25) NOT NULL,
  `alamat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `Nama_admin`, `telp`, `email`, `Username`, `Password`, `alamat`) VALUES
('A0001', 'use', '8798798', 'use@use.umi.ac.id', 'use', 'haha', 'sudiang'),
('A0002', 'Helmy Nirwana', '980980', 'helmy@brigidau', 'el', 'elo', 'rammang');

-- --------------------------------------------------------

--
-- Table structure for table `data_lengkap`
--

CREATE TABLE `data_lengkap` (
  `id_data` char(6) NOT NULL,
  `Hasil` varchar(5) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rule`
--

CREATE TABLE `rule` (
  `id` varchar(10) NOT NULL,
  `X1` varchar(10) NOT NULL,
  `X2` varchar(10) NOT NULL,
  `X3` varchar(10) NOT NULL,
  `X4` varchar(10) NOT NULL,
  `X5` varchar(10) NOT NULL,
  `X6` varchar(10) NOT NULL,
  `X7` varchar(10) NOT NULL,
  `X8` varchar(10) NOT NULL,
  `X9` varchar(10) NOT NULL,
  `X!0` varchar(10) NOT NULL,
  `Y1` varchar(10) NOT NULL,
  `Y2` varchar(10) NOT NULL,
  `Y3` varchar(10) NOT NULL,
  `hasil` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` char(10) NOT NULL,
  `Nama_user` varchar(25) NOT NULL,
  `jk` varchar(2) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `email` varchar(35) NOT NULL,
  `Alamat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `data_lengkap`
--
ALTER TABLE `data_lengkap`
  ADD PRIMARY KEY (`id_data`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `rule`
--
ALTER TABLE `rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_lengkap`
--
ALTER TABLE `data_lengkap`
  ADD CONSTRAINT `data_lengkap_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
