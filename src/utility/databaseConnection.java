package utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author userundie
 */
public class databaseConnection {
        public static Connection dbConnector(){
        Connection c = null;
        try {
            
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver was found");
            c = DriverManager.getConnection("jdbc:mysql://localhost/db_sistempakar", "root", "");
            System.out.println("Database was found");
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(databaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(databaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c;
    }
        
        
}
