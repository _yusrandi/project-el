/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.jfoenix.controls.JFXRadioButton;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author userundie
 */
public class DiagnosaController implements Initializable {

    Connection c;
    PreparedStatement ps;
    ResultSet rs;
    
    @FXML
    private AnchorPane contentPane;
    @FXML
    private JFXRadioButton ya1;
    @FXML
    private JFXRadioButton tidak1;
    @FXML
    private JFXRadioButton ya2;
    @FXML
    private JFXRadioButton tidak2;
    @FXML
    private JFXRadioButton ya3;
    @FXML
    private JFXRadioButton ya4;
    @FXML
    private JFXRadioButton tidak3;
    @FXML
    private JFXRadioButton tidak4;
    @FXML
    private JFXRadioButton ya5;
    @FXML
    private JFXRadioButton ya6;
    @FXML
    private JFXRadioButton ya7;
    @FXML
    private JFXRadioButton ya8;
    @FXML
    private JFXRadioButton ya9;
    @FXML
    private JFXRadioButton ya10;
    @FXML
    private JFXRadioButton ya11;
    @FXML
    private JFXRadioButton ya12;
    @FXML
    private JFXRadioButton ya13;
    @FXML
    private JFXRadioButton ya14;
    @FXML
    private JFXRadioButton tidak5;
    @FXML
    private JFXRadioButton tidak6;
    @FXML
    private JFXRadioButton tidak7;
    @FXML
    private JFXRadioButton tidak8;
    @FXML
    private JFXRadioButton tidak9;
    @FXML
    private JFXRadioButton tidak10;
    @FXML
    private JFXRadioButton tidak11;
    @FXML
    private JFXRadioButton tidak12;
    @FXML
    private JFXRadioButton tidak13;
    @FXML
    private JFXRadioButton tidak14;

    private ToggleGroup s1 = new ToggleGroup();
    private ToggleGroup s2 = new ToggleGroup();
    private ToggleGroup s3 = new ToggleGroup();
    private ToggleGroup s4 = new ToggleGroup();
    private ToggleGroup s5 = new ToggleGroup();
    private ToggleGroup s6 = new ToggleGroup();
    private ToggleGroup s7 = new ToggleGroup();
    private ToggleGroup s8 = new ToggleGroup();
    private ToggleGroup s9 = new ToggleGroup();
    private ToggleGroup s10 = new ToggleGroup();
    private ToggleGroup s11 = new ToggleGroup();
    private ToggleGroup s12 = new ToggleGroup();
    private ToggleGroup s13= new ToggleGroup();
    private ToggleGroup s14= new ToggleGroup();
    
    
    
    public static String x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        checkConnection();
        setToggleGroup();
        // TODO
    }   
    void checkConnection(){
        c = utility.databaseConnection.dbConnector();
        if (c == null) {
            System.out.println("Connection not Succesfull");
            System.exit(1);
        } else {
            System.out.println("Connection Succesfull");
        }
    }
    
    private void setToggleGroup(){
        ya1.setToggleGroup(s1);
        ya2.setToggleGroup(s2);
        ya3.setToggleGroup(s3);
        ya4.setToggleGroup(s4);
        ya5.setToggleGroup(s5);
        ya6.setToggleGroup(s6);
        ya7.setToggleGroup(s7);
        ya8.setToggleGroup(s8);
        ya9.setToggleGroup(s9);
        ya10.setToggleGroup(s10);
        ya11.setToggleGroup(s11);
        ya12.setToggleGroup(s12);
        ya13.setToggleGroup(s13);
        ya14.setToggleGroup(s14);
        
        tidak1.setToggleGroup(s1);
        tidak2.setToggleGroup(s2);
        tidak3.setToggleGroup(s3);
        tidak4.setToggleGroup(s4);
        tidak5.setToggleGroup(s5);
        tidak6.setToggleGroup(s6);
        tidak7.setToggleGroup(s7);
        tidak8.setToggleGroup(s8);
        tidak9.setToggleGroup(s9);
        tidak10.setToggleGroup(s10);
        tidak11.setToggleGroup(s11);
        tidak12.setToggleGroup(s12);
        tidak13.setToggleGroup(s13);
        tidak14.setToggleGroup(s14);
        
    }
    private void getData(){
        if(ya1.isSelected()==true){
            x1 = "ya";
        }else 
            x1 = "tidak";
        
        if(ya2.isSelected()==true){
            x2 = "ya";
        }else x2 = "tidak";
        if(ya3.isSelected()==true){
            x3 = "ya";
        }else x3 = "tidak";
        if(ya4.isSelected()==true){
            x4 = "ya";
        }else x4 = "tidak";
        if(ya5.isSelected()==true){
            x5 = "ya";
        }else x5 = "tidak";
        if(ya6.isSelected()==true){
            x6 = "ya";
        }else x6 = "tidak";
        if(ya7.isSelected()==true){
            x7 = "ya";
        }else x7 = "tidak";
        if(ya8.isSelected()==true){
            x8 = "ya";
        }else x8 = "tidak";
        if(ya9.isSelected()==true){
            x9 = "ya";
        }else x9 = "tidak";
        if(ya10.isSelected()==true){
            x10 = "ya";
        }else x10 = "tidak";
        if(ya11.isSelected()==true){
            x11 = "ya";
        }else x11 = "tidak";
        if(ya12.isSelected()==true){
            x12 = "ya";
        }else x12 = "tidak";
        if(ya13.isSelected()==true){
            x13 = "ya";
        }else x13 = "tidak";
        if(ya14.isSelected()==true){
            x14 = "ya";
        }else x14 = "tidak";
    }

    @FXML
    private void prosesKlik(ActionEvent event) {
        try {
            
            getData();
        
            FXMLLoader loader = new FXMLLoader();
            Parent parent = loader.load(getClass().getResource("/xml/diagnosa_1.fxml"));
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(new Scene(parent)); 
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(DiagnosaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
