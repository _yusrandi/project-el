/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.jfoenix.controls.JFXRadioButton;
import static controller.DiagnosaController.x1;
import static controller.DiagnosaController.x10;
import static controller.DiagnosaController.x11;
import static controller.DiagnosaController.x12;
import static controller.DiagnosaController.x13;
import static controller.DiagnosaController.x14;
import static controller.DiagnosaController.x2;
import static controller.DiagnosaController.x3;
import static controller.DiagnosaController.x4;
import static controller.DiagnosaController.x5;
import static controller.DiagnosaController.x6;
import static controller.DiagnosaController.x7;
import static controller.DiagnosaController.x8;
import static controller.DiagnosaController.x9;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author LENOVO
 */
public class Diagnosa_1Controller implements Initializable {

    Connection c;
    PreparedStatement ps;
    ResultSet rs;
    
    @FXML
    private AnchorPane contentPane;
    @FXML
    private JFXRadioButton ya1;
    @FXML
    private JFXRadioButton tidak1;
    @FXML
    private JFXRadioButton ya2;
    @FXML
    private JFXRadioButton tidak2;
    @FXML
    private JFXRadioButton ya3;
    @FXML
    private JFXRadioButton ya4;
    @FXML
    private JFXRadioButton tidak3;
    @FXML
    private JFXRadioButton tidak4;
    @FXML
    private JFXRadioButton ya5;
    @FXML
    private JFXRadioButton ya6;
    @FXML
    private JFXRadioButton ya7;
    @FXML
    private JFXRadioButton ya8;
    @FXML
    private JFXRadioButton tidak5;
    @FXML
    private JFXRadioButton tidak6;
    @FXML
    private JFXRadioButton tidak7;
    @FXML
    private JFXRadioButton tidak8;
    
    private ToggleGroup s1 = new ToggleGroup();
    private ToggleGroup s2 = new ToggleGroup();
    private ToggleGroup s3 = new ToggleGroup();
    private ToggleGroup s4 = new ToggleGroup();
    private ToggleGroup s5 = new ToggleGroup();
    private ToggleGroup s6 = new ToggleGroup();
    private ToggleGroup s7 = new ToggleGroup();
    private ToggleGroup s8 = new ToggleGroup();
    
    String x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15, x16, x17,x18, x19,
            x20, x21,x22;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        checkConnection();
        setToggleGroup();
        setData();
        // TODO
    }
    
    private void setData(){
        x1 = DiagnosaController.x1;
        x2 = DiagnosaController.x2;
        x3 = DiagnosaController.x3;
        x4 = DiagnosaController.x4;
        x5 = DiagnosaController.x5;
        x6 = DiagnosaController.x6;
        x7 = DiagnosaController.x7;
        x8 = DiagnosaController.x8;
        x9 = DiagnosaController.x9;
        x10 = DiagnosaController.x10;
        x11 = DiagnosaController.x11;
        x12 = DiagnosaController.x12;
        x13 = DiagnosaController.x13;
        x14 = DiagnosaController.x14;
        
    }
    private void getData(){
        if(ya1.isSelected()==true){
            x15 = "ya";
        }else x15 = "tidak";
        if(ya2.isSelected()==true){
            x16 = "ya";
        }else x16 = "tidak";
        if(ya3.isSelected()==true){
            x17 = "ya";
        }else x17 = "tidak";
        if(ya4.isSelected()==true){
            x18 = "ya";
        }else x18 = "tidak";
        if(ya5.isSelected()==true){
            x19 = "ya";
        }else x19 = "tidak";
        if(ya6.isSelected()==true){
            x20 = "ya";
        }else x20 = "tidak";
        if(ya7.isSelected()==true){
            x21 = "ya";
        }else x21 = "tidak";
        if(ya8.isSelected()==true){
            x22 = "ya";
        }else x22 = "tidak";
    }
    
    private void setToggleGroup(){
        ya1.setToggleGroup(s1);
        ya2.setToggleGroup(s2);
        ya3.setToggleGroup(s3);
        ya4.setToggleGroup(s4);
        ya5.setToggleGroup(s5);
        ya6.setToggleGroup(s6);
        ya7.setToggleGroup(s7);
        ya8.setToggleGroup(s8);

        
        tidak1.setToggleGroup(s1);
        tidak2.setToggleGroup(s2);
        tidak3.setToggleGroup(s3);
        tidak4.setToggleGroup(s4);
        tidak5.setToggleGroup(s5);
        tidak6.setToggleGroup(s6);
        tidak7.setToggleGroup(s7);
        tidak8.setToggleGroup(s8);

    }
    void checkConnection(){
        c = utility.databaseConnection.dbConnector();
        if (c == null) {
            System.out.println("Connection not Succesfull");
            System.exit(1);
        } else {
            System.out.println("Connection Succesfull");
        }
    }
    

    @FXML
    private void prosesKlik(ActionEvent event) {
        getData();
        
        System.out.println(x1+x2+x3+x4+x5+x6+x7+x8+x9+x10+x11+x12+x13+x14+x15+x16+x17+x18+x19+x20+x21+x22);
    }
    
}
