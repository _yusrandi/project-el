/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author userundie
 */
public class LoginController implements Initializable {

    @FXML
    private JFXTextField tfUser;
    @FXML
    private JFXTextField tfPass;
    
    Connection c;
    PreparedStatement ps;
    ResultSet rs;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        checkConnection();
    } 
    private void checkConnection(){
        c = utility.databaseConnection.dbConnector();
        if (c == null) {
            System.out.println("Connection not Succesfull");
            System.exit(1);
        } else {
            System.out.println("Connection Succesfull");
        }
    }

    @FXML
    private void signUpKlik(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            Parent parent = loader.load(getClass().getResource("/xml/Signup.fxml"));
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(new Scene(parent));
            stage.show(); 
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    Home_AdminController hac;
    static String admin;
    @FXML
    private void signInKlik(ActionEvent event) {
        try {
            
            String query = "select *from admin where Username=? and Password=?";
            ps = c.prepareStatement(query);
            ps.setString(1, tfUser.getText());
            ps.setString(2, tfPass.getText());
            rs = ps.executeQuery();
            
            
            if(rs.next()){
                admin = tfUser.getText().toString();
                System.out.println("Login success");
                FXMLLoader loader = new FXMLLoader();
                Parent parent = loader.load(getClass().getResource("/xml/Home_Admin.fxml"));
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage.setScene(new Scene(parent));
                stage.show(); 
                
            }else{
                System.out.println("login failed");
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
